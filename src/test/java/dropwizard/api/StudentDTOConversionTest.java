package dropwizard.api;

import static org.junit.Assert.*;

import org.junit.Test;

import dropwizard.core.Student;

public class StudentDTOConversionTest {
	
	private final String NAME_OF_STUDENT = "Seppe";
	private final int AGE_OF_STUDENT = 25;
	
	@Test
	public void testConversionToDTO() {
		StudentDTO studentDTO = createStudentAndConvertToDTO();
		checkIfConversionToDTOIsCorrect(studentDTO);
	}

	private void checkIfConversionToDTOIsCorrect(StudentDTO studentDTO) {
		assertEquals(studentDTO.getAge(), AGE_OF_STUDENT);
		assertEquals(studentDTO.getName(), NAME_OF_STUDENT);	
	}


	private StudentDTO createStudentAndConvertToDTO() {
		Student student = new Student(AGE_OF_STUDENT, NAME_OF_STUDENT);
		return StudentDTO.getDTO(student);
	}

}
