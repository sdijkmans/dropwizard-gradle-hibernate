package dropwizard.resources;

import static org.junit.Assert.*;

import java.io.IOException;

import org.apache.http.HttpResponse;
import org.apache.http.ParseException;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.junit.Test;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import dropwizard.api.StudentDTO;

public class TestStudentResourceAPIWorkflow {

	private final String LOCAL_STUDENT_URL = "http://localhost:8010/student";
	private final String CREATE_STUDENT_NAME = "Seppe";
	private final Integer CREATE_STUDENT_AGE = 25;

	@Test
	public void testCreationOfUserAndFetchingOfSameUser() throws ClientProtocolException, IOException {
		HttpResponse responseOfCreateStudent = doCallToCreationAPIAndReturnResponse();
		StudentDTO retrievedStudentObject = retrieveStudentFromResponse(responseOfCreateStudent);
		checkIfCreatedUserExistsByDoingCallToGetUserByIdAPI(retrievedStudentObject.getId());
	}

	private HttpResponse doCallToGetStudentByIdAPIAndReturnResponse(long id)
			throws ClientProtocolException, IOException {
		HttpUriRequest request = new HttpGet(LOCAL_STUDENT_URL + "?id=" + id);
		return HttpClientBuilder.create().build().execute(request);

	}

	private HttpResponse doCallToCreationAPIAndReturnResponse() throws ClientProtocolException, IOException {
		HttpUriRequest request = new HttpGet(
				LOCAL_STUDENT_URL + "/create?name=" + CREATE_STUDENT_NAME + "&age=" + CREATE_STUDENT_AGE);
		return HttpClientBuilder.create().build().execute(request);
	}

	private void checkIfCreatedUserExistsByDoingCallToGetUserByIdAPI(long id) throws IOException {
		HttpResponse response = doCallToGetStudentByIdAPIAndReturnResponse(id);
		StudentDTO studentDTO = retrieveStudentFromResponse(response);
		checkIfStudentHasCorrectNameAndAge(studentDTO);
	}

	private void checkIfStudentHasCorrectNameAndAge(StudentDTO studentDTO) {
		assertEquals(studentDTO.getName(), CREATE_STUDENT_NAME);
		assertTrue(studentDTO.getAge() == CREATE_STUDENT_AGE);
	}

	private StudentDTO retrieveStudentFromResponse(HttpResponse response) throws IOException {
		String jsonFromReponse = getJsonStringOfHttpResponse(response);
		ObjectMapper mapper = new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		return mapper.readValue(jsonFromReponse, StudentDTO.class);
	}

	private String getJsonStringOfHttpResponse(HttpResponse response) throws ParseException, IOException {
		String jsonFromResponse = EntityUtils.toString(response.getEntity());
		return jsonFromResponse;
	}

}
