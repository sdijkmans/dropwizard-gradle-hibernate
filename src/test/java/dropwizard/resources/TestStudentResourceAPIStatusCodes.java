package dropwizard.resources;

import static org.junit.Assert.*;

import java.io.IOException;

import javax.ws.rs.core.Response.Status;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.HttpClientBuilder;
import org.junit.Test;

public class TestStudentResourceAPIStatusCodes {
	
	private final String LOCAL_STUDENT_URL = 
			"http://localhost:8010/student";

	@Test
	public void testStatusCodeOfRandomAPI() throws ClientProtocolException, IOException {
		HttpUriRequest request = new HttpGet(LOCAL_STUDENT_URL + "/random");
		checkIfRequestReturnsStatusCode(request, Status.OK);
	}
	
	@Test
	public void testStatusCodeOfGetAllStudentsAPI() throws ClientProtocolException, IOException {
		HttpUriRequest request = new HttpGet(LOCAL_STUDENT_URL + "/allstudents");
		checkIfRequestReturnsStatusCode(request, Status.OK);
	}
	
	@Test
	public void testStatusCodeOKOfGetStudentByIdAPI() throws ClientProtocolException, IOException {
		HttpUriRequest request = new HttpGet(LOCAL_STUDENT_URL + "?id=1"); 
		checkIfRequestReturnsStatusCode(request, Status.OK);
	}
	
	@Test
	public void testStatusCode404OfGetStudentByIdAPI() throws ClientProtocolException, IOException {
		HttpUriRequest request = new HttpGet(LOCAL_STUDENT_URL + "?id=-9"); 
		checkIfRequestReturnsStatusCode(request, Status.OK);
	}

	@Test
	public void testStatusCodeOfCreateStudent() throws ClientProtocolException, IOException {
		HttpUriRequest request = new HttpGet(LOCAL_STUDENT_URL + "/create/?name=seppe&age=25"); 
		checkIfRequestReturnsStatusCode(request, Status.OK);
	}
	
	
	@Test
	public void testStatusCodeOfUnknownAPI() throws ClientProtocolException, IOException {
		HttpUriRequest request = new HttpGet(LOCAL_STUDENT_URL + "/nonExisting"); 
		checkIfRequestReturnsStatusCode(request, Status.NOT_FOUND);
	}
	
	
	
	private void checkIfRequestReturnsStatusCode(HttpUriRequest request,Status status) throws ClientProtocolException, IOException {
		HttpResponse httpResponse = HttpClientBuilder.create().build().execute(request);
		assertTrue(httpResponse.getStatusLine().getStatusCode() == status.getStatusCode());
	}

}
