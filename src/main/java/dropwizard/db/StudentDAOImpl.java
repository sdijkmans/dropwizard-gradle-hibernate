package dropwizard.db;

import javax.annotation.Resource;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import org.skife.jdbi.v2.sqlobject.Bind;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import dropwizard.core.Student;
import dropwizard.exceptions.StudentNotFoundException;

import java.util.List;

@Repository
@Resource
public class StudentDAOImpl implements StudentDAO {

	@PersistenceContext
	private EntityManager entityManager;
	
	public StudentDAOImpl() {
	}

	public Student findStudentById(@Bind("id") long id) throws StudentNotFoundException {
		TypedQuery<Student> query = entityManager.createNamedQuery("Student.findStudentById", Student.class);
		query.setParameter("id", id);
		return returnFirstStudent(id,query.getResultList());		
	}

	@Override
	public List<Student> findAllStudents() {
		TypedQuery<Student> query = entityManager.createNamedQuery("Student.findAllStudents", Student.class);
		return query.getResultList();
	}


	@Transactional
	@Override
	public Student createStudent(String name, Integer age) {
		Student student = new Student(age, name);
		entityManager.persist(student);			
		return student;
	}	
	

	private Student returnFirstStudent(long id, List<Student> queryList) throws StudentNotFoundException {
		if (queryList != null && queryList.isEmpty()) {
			throw new StudentNotFoundException("Student with ID [" + id + " not found]");
		} else {
			return (Student) queryList.get(0);
		}
	}
}
