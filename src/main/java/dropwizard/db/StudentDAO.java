package dropwizard.db;

import java.util.List;

import javax.annotation.Resource;

import dropwizard.core.Student;
import dropwizard.exceptions.StudentNotFoundException;

@Resource
public interface StudentDAO {
	
	Student findStudentById(long id) throws StudentNotFoundException;

	List<Student> findAllStudents();
	
	Student createStudent(String name, Integer age);

}
