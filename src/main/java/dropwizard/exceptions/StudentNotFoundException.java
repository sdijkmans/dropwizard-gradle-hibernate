package dropwizard.exceptions;

public class StudentNotFoundException extends Exception {
	private static final long serialVersionUID = 5537467757135340232L;

	public StudentNotFoundException() {
	}

	public StudentNotFoundException(String message) {
		super(message);
	}

	public StudentNotFoundException(Throwable cause) {
		super(cause);
	}

	public StudentNotFoundException(String message, Throwable cause) {
		super(message, cause);
	}
}
