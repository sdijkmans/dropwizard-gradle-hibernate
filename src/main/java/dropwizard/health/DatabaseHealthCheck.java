package dropwizard.health;

import com.codahale.metrics.health.HealthCheck;

import dropwizard.db.StudentDAO;

public class DatabaseHealthCheck extends HealthCheck {
    @SuppressWarnings("unused")
	private final StudentDAO database;

    public DatabaseHealthCheck() {
    	database = null;
    }

    @Override
    protected Result check() throws Exception {    	
    	try {
    		//TODO improve check
    		return Result.healthy();
    	} catch (Exception e) {
    		return Result.unhealthy("couldn't fetch data from database");
    	}
    }
}