package dropwizard.multithreading;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ExecutorServiceProvider {
	
	private ExecutorService executorService;
	
	public ExecutorServiceProvider() {
		initializeService();
	}

	private void initializeService() {
		executorService = Executors.newCachedThreadPool();
	}
	
	public void startRunnable(Runnable runnable) {
		executorService.execute(runnable);
	}

}
