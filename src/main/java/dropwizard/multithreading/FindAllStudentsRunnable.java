package dropwizard.multithreading;

import java.util.List;

import javax.ws.rs.container.AsyncResponse;

import dropwizard.api.StudentDTO;
import dropwizard.core.Student;
import dropwizard.db.StudentDAO;

public class FindAllStudentsRunnable extends StudentDAORunnable {

	public FindAllStudentsRunnable(AsyncResponse asyncRestServiceResponse, StudentDAO studentDAO) {
		super(asyncRestServiceResponse, studentDAO);
	}

	@Override
	public void run() {
		List<Student> studentList =  findAllStudents();
		asyncRestServiceResponse.resume(StudentDTO.getDTO(studentList));
	}
	
	private List<Student> findAllStudents() {
		return studentDAO.findAllStudents();
	}
	

}
