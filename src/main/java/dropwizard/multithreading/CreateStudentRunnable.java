package dropwizard.multithreading;

import javax.ws.rs.container.AsyncResponse;

import dropwizard.api.StudentDTO;
import dropwizard.core.Student;
import dropwizard.db.StudentDAO;

public class CreateStudentRunnable extends StudentDAORunnable {
	
	private String studentName;
	private Integer studentAge;

	public CreateStudentRunnable(AsyncResponse asyncRestServiceResponse, StudentDAO studentDAO, String name, Integer age) {
		super(asyncRestServiceResponse, studentDAO);
		this.studentAge = age;
		this.studentName = name;
	}

	@Override
	public void run() {
		Student student = studentDAO.createStudent(studentName, studentAge);
		asyncRestServiceResponse.resume(StudentDTO.getDTO(student));
	}

}
