package dropwizard.multithreading;

import javax.ws.rs.container.AsyncResponse;
import dropwizard.api.StudentDTO;
import dropwizard.core.Student;
import dropwizard.db.StudentDAO;
import dropwizard.exceptions.StudentNotFoundException;

public class FindStudentWithIdRunnable extends StudentDAORunnable {
	
	private Long studentId;

	public FindStudentWithIdRunnable(AsyncResponse asyncRestServiceResponse, StudentDAO studentDAO, Long id) {
		super(asyncRestServiceResponse, studentDAO);
		this.studentId = id;
	}

	@Override
	public void run() {
		try {
			findStudentById();
		} catch (StudentNotFoundException e) {
			e.printStackTrace();
			sendErrorMessageAsResponse();
		}
	
	}

	private void findStudentById() throws StudentNotFoundException {
		Student foundStudent = studentDAO.findStudentById(studentId);
		asyncRestServiceResponse.resume(StudentDTO.getDTO(foundStudent));
	}
	

}
