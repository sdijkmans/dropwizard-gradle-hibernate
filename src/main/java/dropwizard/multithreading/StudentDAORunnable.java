package dropwizard.multithreading;

import javax.ws.rs.container.AsyncResponse;
import dropwizard.db.StudentDAO;

public abstract class StudentDAORunnable implements Runnable {
	
	protected StudentDAO studentDAO;
	
	protected AsyncResponse asyncRestServiceResponse;
	
	protected final String ERROR_MESSAGE_WHEN_STUDENT_NOT_FOUND = "These are not the students you are looking for";
	
	protected StudentDAORunnable(AsyncResponse asyncRestServiceResponse, StudentDAO studentDAO) {
		this.asyncRestServiceResponse = asyncRestServiceResponse;
		this.studentDAO = studentDAO;
	}
	
	protected void sendErrorMessageAsResponse() {
		asyncRestServiceResponse.resume(ERROR_MESSAGE_WHEN_STUDENT_NOT_FOUND);
	}

	public abstract void run();

}
