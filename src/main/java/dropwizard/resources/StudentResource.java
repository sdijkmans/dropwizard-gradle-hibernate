package dropwizard.resources;

import dropwizard.api.StudentDTO;
import dropwizard.db.StudentDAO;
import dropwizard.multithreading.CreateStudentRunnable;
import dropwizard.multithreading.ExecutorServiceProvider;
import dropwizard.multithreading.FindAllStudentsRunnable;
import dropwizard.multithreading.FindStudentWithIdRunnable;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import com.codahale.metrics.annotation.Timed;

import javax.annotation.Resource;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.container.AsyncResponse;
import javax.ws.rs.container.Suspended;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import java.util.Optional;


@Resource
@Path("/student")
@Api("/student")
@Produces(MediaType.APPLICATION_JSON)
public class StudentResource {

	@Autowired
	private StudentDAO dao;
	
	private ExecutorServiceProvider executorServiceProvider;

	public StudentResource() {
		executorServiceProvider = new ExecutorServiceProvider();
	}

	@GET
	@Timed
	@Path("/random")
	@ApiOperation("Get Random Student")
	public StudentDTO getRandomStudent() {
		return new StudentDTO(1, "Seppe", 25);
	}

	@GET
	@Timed
	@ApiOperation("Get Student with Id")
	public void getNameOfStudentWithId(@QueryParam("id") Optional<Long> id, @Suspended AsyncResponse asyncResponse) throws Exception {
		executorServiceProvider.startRunnable(new FindStudentWithIdRunnable(asyncResponse, dao, id.get()));
	}
	
	@GET
	@Path("/allstudents")
	@ApiOperation("Get list of all students")
	public void getAsyncListOfStudents(@Suspended AsyncResponse asyncResponse) {
		executorServiceProvider.startRunnable(new FindAllStudentsRunnable(asyncResponse, dao));
	}
	
	@GET
	@Path("/create")
	@ApiOperation("Add new Student")
	public void createStudent(@QueryParam("name") String name, @QueryParam("age") Integer age, 
			@Suspended AsyncResponse asyncResponse) {
		executorServiceProvider.startRunnable(new CreateStudentRunnable(asyncResponse, dao, name, age));
	}
}