package dropwizard;

import io.dropwizard.Configuration;
import io.dropwizard.db.DataSourceFactory;
import io.federecio.dropwizard.swagger.SwaggerBundleConfiguration;

import com.fasterxml.jackson.annotation.JsonProperty;
import javax.validation.Valid;
import javax.validation.constraints.*;
import org.hibernate.validator.constraints.NotEmpty;

public class StudentConfiguration extends Configuration {

	
	@JsonProperty("swagger")
    public SwaggerBundleConfiguration swaggerBundleConfiguration;
	
	@Valid
    @NotNull
    @JsonProperty
    private DataSourceFactory database = new DataSourceFactory();
	
    @NotEmpty
    private String template;

    @NotEmpty
    private String defaultName = "Stranger";
    
    @JsonProperty
    private String resourcePackage;

    @JsonProperty
    public String getTemplate() {
        return template;
    }

    @JsonProperty
    public void setTemplate(String template) {
        this.template = template;
    }

    @JsonProperty
    public String getDefaultName() {
        return defaultName;
    }

    @JsonProperty
    public void setDefaultName(String name) {
        this.defaultName = name;
    }
    
    public DataSourceFactory getDatabaseConfiguration() {
        return database;
    }

    @JsonProperty
	public String getResourcePackage() {
		return resourcePackage;
	}

    @JsonProperty
	public void setResourcePackage(String resourcePackage) {
		this.resourcePackage = resourcePackage;
	}
    
    
    
}