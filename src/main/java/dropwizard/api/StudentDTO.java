package dropwizard.api;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import dropwizard.core.Student;

public class StudentDTO {
    private long id;
    private String name;
    private int age;

    public StudentDTO() {
        // Jackson deserialization
    }

    public StudentDTO(long id, String name, int age) {
        this.id = id;
        this.name = name;
        this.age = age;
    }

    public StudentDTO(String name, int age) {
		this.name = name;
		this.age = age;
	}

	@JsonProperty
    public long getId() {
        return id;
    }

    @JsonProperty
    public String getName() {
        return name;
    }
    
    @JsonProperty
    public int getAge() {
        return age;
    }
    
    public static StudentDTO getDTO(Student student) {
    	if(student.getId() == null)
    		return new StudentDTO(student.getName(), student.getAge());
    	return new StudentDTO(student.getId(), student.getName(), student.getAge());
    }
    
    public static List<StudentDTO> getDTO(List<Student> studentList) {
    	List<StudentDTO> studentDTOList = new ArrayList<StudentDTO>();
    	studentList.forEach(s -> studentDTOList.add(getDTO(s)));
    	return studentDTOList;
    }

}