package dropwizard.resources;

import dropwizard.api.StudentDTO;
import dropwizard.core.Student;
import dropwizard.db.StudentDAO;

import com.codahale.metrics.annotation.Timed;

import javax.annotation.Resource;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Optional;

@Path("/student")
@Produces(MediaType.APPLICATION_JSON)
@Resource
public class StudentResource {

	@Autowired
	private StudentDAO dao;

	public StudentResource() {
	}

	@GET
	@Timed
	@Path("/random")
	public StudentDTO getRandomStudent() {
		return new StudentDTO(1, "Seppe", 25);
	}

	@GET
	@Timed
	public StudentDTO getNameOfStudentWithId(@QueryParam("id") Optional<Integer> id) throws Exception {
		Student student = dao.findStudentById(id.get());
		return StudentDTO.getDTO(student);
	}
}