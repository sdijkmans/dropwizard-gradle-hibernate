package dropwizard.db;

import javax.annotation.Resource;

import dropwizard.core.Student;

@Resource
public interface StudentDAO {
	
	Student findStudentById(long id) throws StudentNotFoundException;

}
