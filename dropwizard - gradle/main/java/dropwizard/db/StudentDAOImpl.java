package dropwizard.db;

import javax.annotation.Resource;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.skife.jdbi.v2.sqlobject.Bind;
import org.springframework.stereotype.Repository;

import dropwizard.core.Student;
import java.util.List;

@Repository
@Resource
public class StudentDAOImpl implements StudentDAO {

	@PersistenceContext
	private EntityManager entityManager;
	
	public StudentDAOImpl() {
	}

	public Student findStudentById(@Bind("id") long id) throws StudentNotFoundException {
		TypedQuery<Student> query = entityManager.createNamedQuery("Student.findStudentById", Student.class);
		query.setParameter("id", id);
		return returnFirstStudent(id,query.getResultList());		
	}

	private Student returnFirstStudent(long id, List<Student> queryList) throws StudentNotFoundException {
		if (queryList != null && queryList.isEmpty()) {
			throw new StudentNotFoundException("Student with ID [" + id + " not found]");
		} else {
			return (Student) queryList.get(0);
		}
	}	
}
