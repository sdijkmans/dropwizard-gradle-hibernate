package dropwizard.core;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import static javax.persistence.GenerationType.IDENTITY;

@NamedQueries({
	@NamedQuery(
	name = "Student.findStudentById",
	query = "from Student s where s.id = :id"
	)
})

@Entity
@Table(name="student")
public class Student {
	
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	private Long id;
	
	@Column(name = "age")
	private Integer age;
	
	@Column(name = "name")
	private String name;
		
	public Student() {}
	
	public Student(Long id, Integer age, String name) {
		super();
		this.id = id;
		this.age = age;
		this.name = name;
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Integer getAge() {
		return age;
	}
	public void setAge(Integer age) {
		this.age = age;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	
}
