package dropwizard;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import dropwizard.db.StudentDAOImpl;
import dropwizard.health.DatabaseHealthCheck;
import dropwizard.resources.StudentResource;
import io.dropwizard.Application;
import io.dropwizard.db.DataSourceFactory;
import io.dropwizard.db.ManagedDataSource;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import spring.util.SpringContextBuilder;

public class StudentApplication extends Application<StudentConfiguration> {
	

	public static void main(final String[] args) throws Exception {
		new StudentApplication().run(args);
	}

	@Override
	public String getName() {
		return "Student Rest Service";
	}

	@Override
	public void initialize(final Bootstrap<StudentConfiguration> bootstrap) {
		//initialize
	}

	@Override
	public void run(final StudentConfiguration configuration, final Environment environment) {
		DataSourceFactory dataSourceFactory = configuration.getDatabaseConfiguration();
	    ManagedDataSource dataSource = dataSourceFactory.build(environment.metrics(), "dataSource");
	 
	    AnnotationConfigApplicationContext ctx = new SpringContextBuilder()
	            .addParentContextBean("dataSource", dataSource)
	            .addParentContextBean("configuration", configuration)
	            .addAnnotationConfiguration(StudentSpringConfiguration.class)
	            .build();
			
	    registerBeans(ctx);
		registerResources(environment,ctx);
		registerHealthchecks(environment, ctx);
	}
	
	/**
	 * Registers different resources to dropwizard.
	 * 
	 * @param environment
	 * @param ctx
	 */
	private void registerResources(Environment environment, ApplicationContext ctx) {
		environment.jersey().register(ctx.getBean(StudentResource.class));	
	}
	
	/**
	 * Health checks can be done by going to adminurl/healthchecks.
	 * 
	 * @param environment
	 * @param ctx
	 */
	private void registerHealthchecks(Environment environment, ApplicationContext ctx) {
	    environment.healthChecks().register("DatabaseHealthCheck", new DatabaseHealthCheck());
	}

		
	private void registerBeans(AnnotationConfigApplicationContext ctx) {
		ctx.register(StudentResource.class);
		ctx.register(StudentConfiguration.class);
		ctx.register(StudentDAOImpl.class);		
	}
}
