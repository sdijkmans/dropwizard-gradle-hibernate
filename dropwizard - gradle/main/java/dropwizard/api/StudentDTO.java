package dropwizard.api;

import com.fasterxml.jackson.annotation.JsonProperty;

import dropwizard.core.Student;

public class StudentDTO {
    private long id;
    private String name;
    private int age;

    public StudentDTO() {
        // Jackson deserialization
    }

    public StudentDTO(long id, String name, int age) {
        this.id = id;
        this.name = name;
        this.age = age;
    }

    @JsonProperty
    public long getId() {
        return id;
    }

    @JsonProperty
    public String getName() {
        return name;
    }
    
    @JsonProperty
    public int getAge() {
        return age;
    }
    
    public static StudentDTO getDTO(Student student) {
    	StudentDTO dto = new StudentDTO(student.getId(), student.getName(), student.getAge());
    	return dto;
    }

}