# README #

To start:

1. Build dropwizard in project folder with: 
```
#!gradle 

gradle shadowJar

```

2. Start server with 
```
#!cmd

java -jar build\libs\student-1.0-SNAPSHOT-all.jar server src\main\resources\example.yml

```

For database connection configuration change the settings in src/main/resources/example.yml